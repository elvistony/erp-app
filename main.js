(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _panel_panel_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./panel/panel.component */ "./src/app/panel/panel.component.ts");
/* harmony import */ var _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loginpage/loginpage.component */ "./src/app/loginpage/loginpage.component.ts");






const routes = [
    { path: '', component: _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_3__["LoginpageComponent"] },
    { path: 'panel', component: _panel_panel_component__WEBPACK_IMPORTED_MODULE_2__["PanelComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./loginpage/loginpage.component */ "./src/app/loginpage/loginpage.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class AppComponent {
    constructor() {
        this.title = 'ErpApp';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 2, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-loginpage");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");
    } }, directives: [_loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_1__["LoginpageComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./loginpage/loginpage.component */ "./src/app/loginpage/loginpage.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _panel_panel_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./panel/panel.component */ "./src/app/panel/panel.component.ts");









class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_4__["LoginpageComponent"],
        _panel_panel_component__WEBPACK_IMPORTED_MODULE_7__["PanelComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                    _loginpage_loginpage_component__WEBPACK_IMPORTED_MODULE_4__["LoginpageComponent"],
                    _panel_panel_component__WEBPACK_IMPORTED_MODULE_7__["PanelComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"]
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/loginpage/loginpage.component.ts":
/*!**************************************************!*\
  !*** ./src/app/loginpage/loginpage.component.ts ***!
  \**************************************************/
/*! exports provided: LoginpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginpageComponent", function() { return LoginpageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../user */ "./src/app/user.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _validate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./validate */ "./src/app/loginpage/validate.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");




//Custom Imports




class LoginpageComponent {
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.user = new _user__WEBPACK_IMPORTED_MODULE_2__["User"]();
        this.validator = new _validate__WEBPACK_IMPORTED_MODULE_4__["ERP_Process"];
    }
    ngOnInit() { }
    onSubmit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let one = new Promise((resolve, reject) => { });
            let reply;
            console.log('calling Validate');
            yield _validate__WEBPACK_IMPORTED_MODULE_4__["ERP_Process"].Validate(this.user, this.httpClient);
            console.log('calling GetStudid');
        });
    }
}
LoginpageComponent.ɵfac = function LoginpageComponent_Factory(t) { return new (t || LoginpageComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"])); };
LoginpageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: LoginpageComponent, selectors: [["app-loginpage"]], decls: 9, vars: 2, consts: [[3, "ngSubmit"], ["placeholder", "Username", "name", "u_name", 3, "ngModel", "ngModelChange"], ["placeholder", "Password", "name", "pass", 3, "ngModel", "ngModelChange"], ["type", "submit", "value", "Login", "name", "submit"]], template: function LoginpageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "form", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function LoginpageComponent_Template_form_ngSubmit_0_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Username:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "input", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function LoginpageComponent_Template_input_ngModelChange_3_listener($event) { return ctx.user.u_name = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Password:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function LoginpageComponent_Template_input_ngModelChange_7_listener($event) { return ctx.user.pass = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](8, "input", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.user.u_name);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.user.pass);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2lucGFnZS9sb2dpbnBhZ2UuY29tcG9uZW50LmNzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](LoginpageComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
                selector: 'app-loginpage',
                templateUrl: './loginpage.component.html',
                styleUrls: ['./loginpage.component.css']
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/loginpage/validate.ts":
/*!***************************************!*\
  !*** ./src/app/loginpage/validate.ts ***!
  \***************************************/
/*! exports provided: ERP_Process */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERP_Process", function() { return ERP_Process; });
/* harmony import */ var reflect_metadata__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! reflect-metadata */ "./node_modules/reflect-metadata/Reflect.js");
/* harmony import */ var reflect_metadata__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(reflect_metadata__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var dataframe_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dataframe-js */ "./node_modules/dataframe-js/lib/index.js");
/* harmony import */ var dataframe_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dataframe_js__WEBPACK_IMPORTED_MODULE_1__);


class ERP_Process {
    constructor() { }
    static Validate(user, httpClient) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let url = 'https://erp.vidyaacademy.ac.in/web/session/authenticate';
        let login = {
            'jsonrpc': "2.0",
            'method': "call",
            'params': {
                "db": "liveone",
                "login": user.u_name,
                "password": user.pass,
                "base_location": "https://erp.vidyaacademy.ac.in",
                "session_id": "",
                "context": {}
            },
            'db': "liveone",
            'login': user.u_name,
            'password': user.pass,
            'base_location': "https://erp.vidyaacademy.ac.in",
            'session_id': "",
            'context': {},
            'id': "r7"
        };
        //console.log(login)
        let reply;
        httpClient.post(url, login, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            let r = JSON.parse(JSON.stringify(res.body));
            user.session_id = r['result']['session_id'];
            user.uid = r['result']['uid'];
            console.log('Validation Success ');
            console.log("Session:" + user.session_id);
            console.log("UID:" + user.uid);
            reply = JSON.stringify(res.body).toString();
            // console.log(res.headers)
        }).then(() => {
            ERP_Process.getStudId(user, httpClient);
        });
    }
    static getStudId(user, httpClient) {
        let url = 'https://erp.vidyaacademy.ac.in/web/dataset/call_kw';
        let getstud_id = {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "vict.academics.duty.leave.status",
                "method": "default_get",
                "args": [
                    [
                        "name",
                        "student_id",
                    ]
                ],
                "kwargs": {
                    "context": {
                        "lang": "en_GB",
                        "tz": "Asia/Kolkata",
                        "uid": user.uid
                    }
                },
                "session_id": user.session_id,
                "context": {
                    "lang": "en_GB", "tz": "Asia/Kolkata",
                    "uid": user.uid
                }
            }, "id": "r36"
        };
        //console.log(login)
        // console.log('GetStudID heard call')
        // console.log(JSON.stringify(getstud_id))        
        httpClient.post(url, getstud_id, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            // console.log(res.body)
            let r = JSON.parse(JSON.stringify(res.body));
            user.stud_id = r['result']['student_id'];
            console.log('Student ID Retrieval: ');
            console.log("Student ID:" + user.stud_id);
        }).then(() => {
            ERP_Process.getViewID(user, httpClient);
        });
    }
    static getViewID(user, httpClient) {
        let url = 'https://erp.vidyaacademy.ac.in/web/dataset/call_kw';
        let getstud_id = {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "vict.academics.check.student.attendance",
                "method": "create",
                "args": [
                    {
                        "student_id": user.stud_id,
                        "state": "draft",
                        "company_id": 21,
                        "from_date": null,
                        "to_date": null,
                        "select_course": null,
                        "course": null
                    }
                ],
                "kwargs": {
                    "context": {
                        "lang": "en_GB",
                        "tz": "Asia/Kolkata",
                        "uid": user.uid
                    }
                },
                "session_id": user.session_id,
                "context": {
                    "lang": "en_GB",
                    "tz": "Asia/Kolkata",
                    "uid": user.uid
                }
            },
            "id": "r231"
        };
        httpClient.post(url, getstud_id, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            // console.log(res.body)
            let r = JSON.parse(JSON.stringify(res.body));
            user.view_id = r['result'];
            //console.log(r)
            console.log('View ID Retrieval: ');
            console.log("View ID:" + user.view_id);
        })
            .then(() => {
            ERP_Process.RetrieveCourse(user, httpClient, 1);
        });
    }
    static RetrieveCourse(user, httpClient, n) {
        let url = 'https://erp.vidyaacademy.ac.in/web/dataset/call_kw';
        let getstud_id = {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "vict.academics.check.student.attendance",
                "method": "read",
                "args": [
                    [
                        user.view_id
                    ],
                    [
                        "atten_status",
                        "branch_options",
                        "present_no",
                        "to_date",
                        "student_first_name",
                        "sub_period_id",
                        "university_no",
                        "student_id",
                        "percentage",
                        "select_course",
                        "absent_no",
                        "from_date",
                        "name",
                        "course",
                        "student_last_name",
                        "academic_period_id",
                        "display_name"
                    ]
                ],
                "kwargs": {
                    "context": {
                        "lang": "en_GB",
                        "tz": "Asia/Kolkata",
                        "uid": user.uid,
                        "bin_size": true,
                        "future_display_name": true
                    }
                },
                "session_id": user.session_id,
                "context": {
                    "lang": "en_GB",
                    "tz": "Asia/Kolkata",
                    "uid": user.uid
                }
            },
            "id": "r233"
        };
        httpClient.post(url, getstud_id, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            // console.log(res.body)
            let r = JSON.parse(JSON.stringify(res.body));
            //console.log(r['result'][0])
            user.firstname = r['result'][0]['student_first_name'];
            user.branch = r['result'][0]['branch_options'];
            user.courses_ids = r['result'][0]['atten_status'];
            console.log('Blank Call');
        })
            .then(() => {
            if (n == 1) {
                ERP_Process.CallButton(user, httpClient);
            }
            else {
                ERP_Process.GetCourseData(user, httpClient);
            }
        });
    }
    static CallButton(user, httpClient) {
        let url = 'https://erp.vidyaacademy.ac.in/web/dataset/call_button';
        let getstud_id = {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "vict.academics.check.student.attendance",
                "method": "button_check_status",
                "domain_id": "None",
                "context_id": 1,
                "args": [
                    [
                        user.view_id
                    ],
                    {
                        "lang": "en_GB",
                        "tz": "Asia/Kolkata",
                        "uid": user.uid
                    }
                ],
                "session_id": user.session_id,
                "context": {
                    "lang": "en_GB",
                    "tz": "Asia/Kolkata",
                    "uid": user.uid
                }
            },
            "id": "r234"
        };
        httpClient.post(url, getstud_id, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            // console.log(res.body)
            let r = JSON.parse(JSON.stringify(res.body));
            //console.log(r['result'])
            console.log('Button Press Simulated Retrieval: ');
        })
            .then(() => {
            ERP_Process.RetrieveCourse(user, httpClient, 2);
        });
    }
    static GetCourseIDs(user, httpClient) {
        let url = 'https://erp.vidyaacademy.ac.in/web/dataset/call_kw';
        let getstud_id = {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "vict.academics.check.student.atten.lines",
                "method": "read",
                "args": [
                    user.courses_ids,
                    [
                        "marking_date",
                        "hour",
                        "course",
                        "marked_faculty_name",
                        "attendance_state"
                    ]
                ],
                "kwargs": {
                    "context": {
                        "lang": "en_GB",
                        "tz": "Asia/Kolkata",
                        "uid": user.uid
                    }
                },
                "session_id": user.session_id,
                "context": {
                    "lang": "en_GB",
                    "tz": "Asia/Kolkata",
                    "uid": user.uid
                }
            },
            "id": "r238"
        };
        httpClient.post(url, getstud_id, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            // console.log(res.body)
            let r = JSON.parse(JSON.stringify(res.body));
            // user.courses=r['result'][]
            //console.log(r)
            console.log("Getting Course ID:");
            console.log(user.courses_ids);
        })
            .then(() => {
            ERP_Process.GetCourseData(user, httpClient);
        });
    }
    static GetCourseData(user, httpClient) {
        let url = 'https://erp.vidyaacademy.ac.in/web/dataset/call_kw';
        let getstud_id = {
            "jsonrpc": "2.0",
            "method": "call",
            "params": {
                "model": "vict.academics.check.student.atten.lines",
                "method": "read",
                "args": [
                    user.courses_ids,
                    [
                        "marking_date",
                        "hour",
                        "course",
                        "marked_faculty_name",
                        "attendance_state"
                    ]
                ],
                "kwargs": {
                    "context": {
                        "lang": "en_GB",
                        "tz": "Asia/Kolkata",
                        "uid": user.uid
                    }
                },
                "session_id": user.session_id,
                "context": {
                    "lang": "en_GB",
                    "tz": "Asia/Kolkata",
                    "uid": user.uid
                }
            },
            "id": "r238"
        };
        httpClient.post(url, getstud_id, { observe: 'response', withCredentials: true }).toPromise().then(res => {
            // console.log(res.body)
            let r = JSON.parse(JSON.stringify(res.body));
            // user.courses=r['result'][]
            //console.log(getstud_id)
            //console.log(r)
            console.log('Getting Attendance');
            let df = new dataframe_js__WEBPACK_IMPORTED_MODULE_1___default.a(r['result']);
            let course = df.select('course').dropDuplicates('course');
            course.show();
            let row;
            let coursenames = [];
            for (row of course.toArray()) {
                coursenames.push(row[0][1]);
            }
            // console.log(course.count())
            // console.log(course.getRow(1).toArray()[0])
            console.log(coursenames);
            // console.log(course.toArray())
        });
        // .then(()=>{
        //     ERP_Process.getStudId(user,httpClient)
        // })
    }
}


/***/ }),

/***/ "./src/app/panel/panel.component.ts":
/*!******************************************!*\
  !*** ./src/app/panel/panel.component.ts ***!
  \******************************************/
/*! exports provided: PanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PanelComponent", function() { return PanelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class PanelComponent {
    constructor() { }
    ngOnInit() {
    }
}
PanelComponent.ɵfac = function PanelComponent_Factory(t) { return new (t || PanelComponent)(); };
PanelComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PanelComponent, selectors: [["app-panel"]], decls: 2, vars: 0, template: function PanelComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "panel works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhbmVsL3BhbmVsLmNvbXBvbmVudC5jc3MifQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PanelComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-panel',
                templateUrl: './panel.component.html',
                styleUrls: ['./panel.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/user.ts":
/*!*************************!*\
  !*** ./src/app/user.ts ***!
  \*************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
class User {
    constructor() {
        this.u_name = '';
        this.pass = '';
    }
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /media/lvz-tony/Shared-Drive/projects/angular/ErpApp/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map